#!/usr/bin/python3
import cgi
import csv

pedido = cgi.FieldStorage()

login = pedido.getvalue('login')
senha = pedido.getvalue('senha')
assinar = pedido.getvalue('assinar')


print('Content-type: text/html; charset=utf-8\n\n')

with open('assinantes.csv', 'a') as assinantes:
    registro = f'{login},{senha},{assinar}\n'
    assinantes.write(registro)

with open('assinantes.csv') as arq:
    for linha in arq:    
        registro = linha.split(',')

        login = registro[0]
        senha = registro[1]
        assinar = registro[2]

with open('assinantes.csv') as arq:
    assinantes = '<ol>\n'
    for linha in arq:
        assinantes += '<li>'+linha+'</li>\n'

    assinantes += '</ol>\n'


    parte1 = f'''
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <h1>Lista dos assinantes</h1>
            {login}
            </form>
        </body>
    </html>
    '''

    print(parte1)

    parte2 = f'''
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <h1>Lista de senhas</h1>
            {senha}
            </form>
        </body>
    </html>
    '''

    print(parte2)

    parte3 = f'''
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <h1>Se assinou</h1>
            {assinar}
            </form>
        </body>
    </html>
    '''

    print(parte3)